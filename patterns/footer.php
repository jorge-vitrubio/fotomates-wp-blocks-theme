<?php
/**
 * Title: footer
 * Slug: fotomates/footer
 * Categories: hidden
 * Inserter: no
 */
?>
<!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:var(--wp--preset--spacing--50);padding-bottom:var(--wp--preset--spacing--50)"><!-- wp:separator {"align":"full","style":{"spacing":{"margin":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50"}}}} -->
<hr class="wp-block-separator alignfull has-alpha-channel-opacity" style="margin-top:var(--wp--preset--spacing--50);margin-bottom:var(--wp--preset--spacing--50)"/>
<!-- /wp:separator -->

<!-- wp:group {"align":"wide","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group alignwide"><!-- wp:paragraph -->
<p><?php echo __('Entitats Col·laboradores:', 'fotomates');?></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"wrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:image {"width":"auto","height":"60px","linkDestination":"custom"} -->
<figure class="wp-block-image is-resized"><a href="https://www.barcelona.cat/ca/" target="_blank" rel="noreferrer noopener"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/AjuntamentBarcelonaFonsGris.jpg" alt="" style="width:auto;height:60px"/></a></figure>
<!-- /wp:image -->

<!-- wp:image {"width":"auto","height":"60px","linkDestination":"custom"} -->
<figure class="wp-block-image is-resized"><a href="https://cims-cellex.cat/" target="_blank" rel="noreferrer noopener"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/CimsCellx.jpg" alt="" style="width:auto;height:60px"/></a></figure>
<!-- /wp:image -->

<!-- wp:image {"width":"auto","height":"60px","linkDestination":"custom"} -->
<figure class="wp-block-image is-resized"><a href="https://www.casio-europe.com/es/" target="_blank" rel="noreferrer noopener"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/CasioFonsGris.jpg" alt="" style="width:auto;height:60px"/></a></figure>
<!-- /wp:image -->

<!-- wp:image {"width":"auto","height":"60px","linkDestination":"custom"} -->
<figure class="wp-block-image is-resized"><a href="https://www.iefc.cat/" target="_blank" rel="noreferrer noopener"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/<?php echo __('IEFC', 'fotomates');?>LogoGris.jpg" alt="<?php echo __('IEFC', 'fotomates');?>" style="width:auto;height:60px"/></a></figure>
<!-- /wp:image -->

<!-- wp:image {"width":"auto","height":"60px","linkDestination":"custom"} -->
<figure class="wp-block-image is-resized"><a href="https://www.upc.edu/ca" target="_blank" rel="noreferrer noopener"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/<?php echo __('UPC', 'fotomates');?>LogoFonsGris.jpg" alt="<?php echo __('UPC', 'fotomates');?>" style="width:auto;height:60px"/></a></figure>
<!-- /wp:image -->

<!-- wp:image {"width":"auto","height":"60px","linkDestination":"custom"} -->
<figure class="wp-block-image is-resized"><a href="https://www.barcanova.cat/" target="_blank" rel="noreferrer noopener"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/<?php echo __('Barcanova', 'fotomates');?>FonsGris.jpg" alt="<?php echo __('Barcanova', 'fotomates');?>" style="width:auto;height:60px"/></a></figure>
<!-- /wp:image -->

<!-- wp:image {"width":"auto","height":"60px","linkDestination":"custom"} -->
<figure class="wp-block-image is-resized"><a href="https://www.casanovafoto.com/" target="_blank" rel="noreferrer noopener"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/CasanovafotoLogoGris.jpg" alt="<?php echo __('Casanova Foto', 'fotomates');?>" style="width:auto;height:60px"/></a></figure>
<!-- /wp:image -->

<!-- wp:image {"width":"auto","height":"60px","linkDestination":"custom"} -->
<figure class="wp-block-image is-resized"><a href="http://fotobit.com/" target="_blank" rel="noreferrer noopener"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/FotoBitLogoGris.jpg" alt="<?php echo __('Fotobit', 'fotomates');?>" style="width:auto;height:60px"/></a></figure>
<!-- /wp:image -->

<!-- wp:image {"width":"auto","height":"60px","linkDestination":"custom"} -->
<figure class="wp-block-image is-resized"><a href="https://www.abacus.coop/ca/home?resetlanguage=true" target="_blank" rel="noreferrer noopener"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/<?php echo __('Abacus', 'fotomates');?>FonsGris-1.jpg" alt="<?php echo __('Abacus', 'fotomates');?>" style="width:auto;height:60px"/></a></figure>
<!-- /wp:image -->

<!-- wp:image {"width":"auto","height":"60px","linkDestination":"custom"} -->
<figure class="wp-block-image is-resized"><a href="https://mmaca.cat/" target="_blank" rel="noreferrer noopener"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/logo_<?php echo __('MMACA', 'fotomates');?>FonsGris.png" alt="<?php echo __('MMACA', 'fotomates');?>" style="width:auto;height:60px"/></a></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:separator {"align":"full","style":{"spacing":{"margin":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50"}}}} -->
<hr class="wp-block-separator alignfull has-alpha-channel-opacity" style="margin-top:var(--wp--preset--spacing--50);margin-bottom:var(--wp--preset--spacing--50)"/>
<!-- /wp:separator -->

<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"width":"40%"} -->
<div class="wp-block-column" style="flex-basis:40%"><!-- wp:group {"style":{"dimensions":{"minHeight":""},"layout":{"selfStretch":"fit","flexSize":null}},"layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group"><!-- wp:site-title {"level":0,"fontSize":"medium"} /-->

<!-- wp:site-tagline {"fontSize":"small"} /--></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"30%"} -->
<div class="wp-block-column" style="flex-basis:30%"><!-- wp:navigation {"overlayMenu":"never","layout":{"type":"flex","orientation":"vertical"}} /--></div>
<!-- /wp:column -->

<!-- wp:column {"width":"30%"} -->
<div class="wp-block-column" style="flex-basis:30%"><!-- wp:group {"layout":{"type":"flex","orientation":"vertical","justifyContent":"right"}} -->
<div class="wp-block-group"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"className":"has-medium-font-size","fontFamily":"body"} -->
<h2 class="wp-block-heading has-medium-font-size has-body-font-family" style="font-style:normal;font-weight:600"><?php echo __('Xarxes socials', 'fotomates');?></h2>
<!-- /wp:heading -->

<!-- wp:navigation {"overlayMenu":"never","layout":{"type":"flex","orientation":"vertical","justifyContent":"right"}} /--></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:separator {"align":"full","style":{"spacing":{"margin":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50"}}}} -->
<hr class="wp-block-separator alignfull has-alpha-channel-opacity" style="margin-top:var(--wp--preset--spacing--50);margin-bottom:var(--wp--preset--spacing--50)"/>
<!-- /wp:separator -->

<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"0","bottom":"0"}}}} -->
<div class="wp-block-group alignwide" style="padding-top:0;padding-bottom:0"><!-- wp:heading {"level":5,"style":{"elements":{"link":{"color":{"text":"var:preset|color|contrast"}}}},"textColor":"contrast-2","fontSize":"small"} -->
<h5 class="wp-block-heading has-contrast-2-color has-text-color has-link-color has-small-font-size"><?php echo __('Llicència', 'fotomates');?></h5>
<!-- /wp:heading -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:paragraph -->
<p><?php echo __('Les imatges i els continguts d\'aquesta web estan sota llicència <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank" rel="noreferrer noopener">Creative Commons</a>. Pots reproduir-los, distribuir-los i modificar-los sempre que citis l\'autor i el lloc de procedència. La distribució s\'ha de fer sota la mateixa llicència CC o equivalent, i no es permet l\'ús comercial dels continguts ni de les possibles obres derivades.', 'fotomates');?></p>
<!-- /wp:paragraph -->

<!-- wp:image {"linkDestination":"custom","align":"center"} -->
<figure class="wp-block-image aligncenter"><a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank" rel="noreferrer noopener"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/assets/images/CreativeCommonsGran.png" alt="" class=""/></a></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:separator {"align":"full","style":{"spacing":{"margin":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50"}}}} -->
<hr class="wp-block-separator alignfull has-alpha-channel-opacity" style="margin-top:var(--wp--preset--spacing--50);margin-bottom:var(--wp--preset--spacing--50)"/>
<!-- /wp:separator -->

<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"0","bottom":"0"}}}} -->
<div class="wp-block-group alignwide" style="padding-top:0;padding-bottom:0"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|contrast"}}}},"textColor":"contrast-2","fontSize":"small"} -->
<p class="has-contrast-2-color has-text-color has-link-color has-small-font-size"><?php echo __(' Dissenyat iprogramat per <a href="https://vitrubio.net">vitrubio.net</a> i fet amb <a href="https://ca.wordpress.org" rel="nofollow">WordPress</a> ', 'fotomates');?></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|contrast"}}}},"textColor":"contrast-2","fontSize":"small"} -->
<p class="has-contrast-2-color has-text-color has-link-color has-small-font-size"><?php echo __('Un projecte de l\'<a href="http://abeam.feemcat.org/">ABEAM</a> amb llicencia llure <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC-BY-NC-SA</a> ', 'fotomates');?></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->