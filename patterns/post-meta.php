<?php
/**
 * Title: post-meta
 * Slug: fotomates/post-meta
 * Categories: hidden
 * Inserter: no
 */
?>
<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"0.3em"}},"layout":{"type":"flex","justifyContent":"left"}} -->
<div class="wp-block-group"><!-- wp:post-date {"format":"M j, Y","isLink":true} /-->

<!-- wp:paragraph {"textColor":"contrast-2"} -->
<p class="has-contrast-2-color has-text-color"><?php echo __('—', 'fotomates');?></p>
<!-- /wp:paragraph -->

<!-- wp:post-terms {"term":"category","prefix":"en "} /--></div>
<!-- /wp:group --></div>
<!-- /wp:group -->