<?php
/**
 * Lliurament form Block template.
 *
 * @param array $block The block settings and attributes.
 */
?>
<?php
  // this form is protected in the template with swpm-partial-protection plugin
  // if placed anywere else please... protect it from public publishing
?>
<?php acf_form_head(); ?>
<div id="formulari-lliurament" class="lliurament-form">
<?php
  // https://www.advancedcustomfields.com/resources/acf_form/#parameters
  // https://developer.wordpress.org/reference/functions/wp_insert_post/
   acf_form(array(
     'post_id'       => 'new_post',
     'new_post'      => array(
       'post_type'     => 'fotomates-lliurament',
       //'post_status'   => 'draft' //'publish' 'pending'
       //'post_category' => array(),
       //'tags_input'    => array(),
       //'tax_input'     => array( 
           // $taxonomy => $tags 
           //'fotomates-lliurament-category' => $taxonomy_ids
       //),
       //'meta_input' => array( ),
     ),
     //'field_groups'  => false,
     //'fields'        => false,
     'post_title'    => true,
     'post_content'  => false,
     //'form'          => true,
     //'form_attributes' => array(),
     'return'        => home_url('participa/perfil-participant/'), //'%post_url%'
     //'html_before_fields' => '',
     //'html_after_fields' => '',
     'submit_value'  =>__("Enviar", 'acf'),
     'updated_message' => __("Contingut enviat", 'acf'),
     'label_placement' => 'top', //'left' (besides fields)
     'instruction_placement' => 'label', //'field' (below fields)
     'field_el'      => 'div', // ‘div’, ‘tr’, ‘td’, ‘ul’, ‘ol’, 
     'uploader'      => 'wp', // 'basic'
     'honeypot'      => true, //
     'html_updated_message'  => '<div id="message" class="updated success"><p>%s</p></div>',
     'html_submit_spinner' => '<span class="acf-spinner"></span>',
     'kses'          => true
   ));
?>
</div>
